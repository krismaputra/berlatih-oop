<?php 

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$animal = new Animal("shaun");



echo "Nama : $animal->name <br>"; // shaun
echo "Jumlah Kaki : $animal->legs <br>"; // 2
echo "Cold Blood : $animal->blood <br><br>"; // false

$frog = new Frog("frog");
echo "Nama : $frog->name <br>"; 
echo "Jumlah Kaki : $frog->legs <br>"; 
echo "Cold Blood : $frog->blood <br>";
echo $frog->jump();
echo "<br><br>";

$ape = new Ape("Kera Sakti");
echo "Nama : $ape->name <br>"; 
echo "Jumlah Kaki : $ape->legs <br>"; 
echo "Cold Blood : $ape->blood <br>";
echo $ape->yell();
echo "<br>";

?>